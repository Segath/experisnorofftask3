﻿using System;

namespace Task8_DrawSquareMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            int squareSize = UserInput();
            DrawSquare(squareSize);
        }

        public static int UserInput()
        {
            int squareSize = 0;
            do
            {
                Console.Write("Type in the requested size of the square: ");
                string inputValue = Console.ReadLine();
                try
                {
                    squareSize = int.Parse(inputValue);
                }
                catch (FormatException)
                {
                    Console.WriteLine("ERROR: Write a value, not a character");
                }
            } while (squareSize <= 0);
            return squareSize;
        }

        public static void DrawSquare(int squareSize)
        {
            // Row
            for (int i = 0; i < squareSize; i++)
            {
                // First and Last row, draw full line
                if (i == 0 || i == squareSize - 1)
                {
                    for (int j = 0; j < squareSize; j++)
                    {
                        Console.Write("#");
                    }
                }

                // Draw a point at the edges of the square
                else
                {
                    for (int k = 0; k < squareSize; k++)
                    {
                        if (k == 0 || k == squareSize - 1)
                        {
                            Console.Write("#");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                    }
                }
                Console.WriteLine();
            }
        }

    }
}
